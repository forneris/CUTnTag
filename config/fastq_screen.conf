# This is an example configuration file for FastQ Screen

############################
## Bowtie, Bowtie 2 or BWA #
############################
## If the Bowtie, Bowtie 2 or BWA binary is not in your PATH, you can set 
## this value to tell the program where to find your chosen aligner.  Uncomment 
## the relevant line below and set the appropriate location.  Please note, 
## this path should INCLUDE the executable filename.

#BOWTIE	/usr/local/bin/bowtie/bowtie
BOWTIE2 /g/furlong/forneris/software/Anaconda3/envs/bowtie2/bin/bowtie2
#BWA /usr/local/bwa/bwa



############################################
## Bismark (for bisulfite sequencing only) #
############################################
## If the Bismark binary is not in your PATH then you can set this value to 
## tell the program where to find it.  Uncomment the line below and set the 
## appropriate location. Please note, this path should INCLUDE the executable 
## filename.

#BISMARK	/usr/local/bin/bismark/bismark



############
## Threads #
############
## Genome aligners can be made to run across multiple CPU cores to speed up 
## searches.  Set this value to the number of cores you want for mapping reads.

THREADS		8



##############
## DATABASES #
##############
## This section enables you to configure multiple genomes databases (aligner index 
## files) to search against in your screen.  For each genome you need to provide a 
## database name (which can't contain spaces) and the location of the aligner index 
## files.
##
## The path to the index files SHOULD INCLUDE THE BASENAME of the index, e.g:
## /data/public/Genomes/Human_Bowtie/GRCh37/Homo_sapiens.GRCh37
## Thus, the index files (Homo_sapiens.GRCh37.1.bt2, Homo_sapiens.GRCh37.2.bt2, etc.) 
## are found in a folder named 'GRCh37'.
##
## If, for example, the Bowtie, Bowtie2 and BWA indices of a given genome reside in 
## the SAME FOLDER, a SINLGE path may be provided to ALL the of indices.  The index 
## used will be the one compatible with the chosen aligner (as specified using the 
## --aligner flag).  
##
## The entries shown below are only suggested examples, you can add as many DATABASE 
## sections as required, and you can comment out or remove as many of the existing 
## entries as desired.  We suggest including genomes and sequences that may be sources 
## of contamination either because they where run on your sequencer previously, or may 
## have contaminated your sample during the library preparation step.
##
## Melanogaster
DATABASE	Melanogaster	/g/furlong/genome/D.melanogaster/Dm6/indexes/bowtie2/dm6.UCSC.noMask
##
## Virilis
DATABASE	Virilis	/g/furlong/genome/D.virilis/R1.2/indexes/bowtie2/d_virilis_fb_R1.2
##
## Wolbachia
DATABASE        Wolbachia       /g/furlong/genome/Wolbachia.pipientis.wMel/GCA_000008025.1/index/bowtie2/AE017196
##
## E coli
DATABASE	E.coli	/g/furlong/genome/E.coli/index/bowtie2/Escherichia_coli_strain_97-3250
##
## Rhodococcus erythropolis
DATABASE	R.erythropolis	/g/furlong/genome/R.erythropolis/index/bowtie2/Rhodococcus_erythropolis_R138
##
## Bacillus subtilis
DATABASE	B.subtilis	/g/furlong/genome/B.subtilis/index/bowtie2/Bacillus_subtilis_NC_000964.3 
##
## Staphilococcus aureus
DATABASE	S.aureus	/g/furlong/genome/S.aureus/index/bowtie2/Staphylococcus_aureus_NC_007795.1
##
## Staphilococcus epidermidis
DATABASE	S.epidermidis	/g/furlong/genome/S.epidermidis/index/bowtie2/Staphylococcus_epidermidis_NZ_CP035288.1
##
## Lambda
DATABASE	Lambda	/g/furlong/genome/Lambda/index/bowtie2/Lambda
##
## Vectors
DATABASE	Vectors	/g/furlong/genome/Vectors/index/bowtie2/Vectors
##
## Mitochondria collection
DATABASE	Mitochondria	/g/furlong/genome/Mitochondria_collection/index/bowtie2/mitochondria
##
## rRNAs collection
DATABASE	rRNAs	/g/furlong/genome/rRNAs_collection/index/bowtie2/GRCm38_rRNA
##
## Mouse 
DATABASE	Mouse	/g/furlong/genome/M.musculus/mm10/index/bowtie2/Mus_musculus.GRCm38.dna.primary_assembly
##
## Human - sequences available from
DATABASE	Human	/g/furlong/genome/H.sapiens/GRCh38/index/bowtie2/Homo_sapiens.GRCh38.dna.toplevel
##
## PhiX 
DATABASE	PhiX	/g/furlong/genome/phix/bowtie2/phix174
##

